FROM ubuntu:18.04

RUN apt-get update && apt-get install -y \
    nginx \
    php-fpm

COPY ./site.conf /etc/nginx/conf.d/site.conf
COPY ./code /code
COPY ./script /script

ENTRYPOINT /script